**Convolutional Neural Network**

In deep learning, a convolutional neural network (CNN) is a class of deep neural networks, most commonly applied to analyzing visual imagery. They are also known as shift invariant or space invariant artificial neural networks (SIANN), based on their shared-weights architecture and translation invariance characteristics.

[CNN](https://miro.medium.com/max/3288/1*uAeANQIOQPqWZnnuH-VEyw.jpeg)

**Important Terms**

i) **Kernel size (K):** kernel size refers to the width x height of the filter.

ii) **Stride (S):** stride is the number of pixels shifts over the input matrix.

iii) **Zero padding (pad):** it refers to the process of symmetrically adding zeroes to the input matrix.

iv) **Number of filters (F):** it refers to the no. of neurons or to be more precise it is the neuron,s input weights form.

v) **Flattening :** it is converting the data into a 1 dimensional array for inputting it to the next layer.


**How Does CNN Work?**

A convolutional neural network can have tens or hundreds of layers that each learn to detect different features of an image. Filters are applied to each training image at different resolutions, and the output of each convolved image is used as the input to the next layer. The filters can start as very simple features, such as brightness and edges, and increase in complexity to features that uniquely define the object.

CNNs perform feature identification and classification of images, text, sound, and video.